#!/bin/bash

### if [ ! -x /usr/bin/npm ]; then
###   curl -fsSL 'https://deb.nodesource.com/setup_15.x' | bash
###   apt-get install -y nodejs
### fi
### 
### test -d ~/node_modules || mkdir ~/node_modules
### 
### cd /vagrant
### if [ ! -e node_modules ]; then
###   ln -s ~/node_modules node_modules # Линкуем node_modules внутрь виртуальной машины.
### fi
### # Простой способ обойти ограничение на создание символических ссылок в общей директории.
### npm init -y fastify
### npm install
### #FASTIFY_ADDRESS=0.0.0.0 npm run dev # проверяем что все работает
### # open localhost:3000

#apt-get update
#apt-get update

if [ ! -d ~/.asdf ]; then
	git clone https://github.com/asdf-vm/asdf.git ~/.asdf --branch v0.8.0
fi
export PATH="$PATH:$HOME/.asdf/bin"

cat > ~/.bash_profile <<_EOF
if [ -f "$HOME/.asdf/asdf.sh" ]; then
	. "$HOME/.asdf/asdf.sh"
fi

if [ -f "$HOME/completions/asdf.bash" ]; then
	. "$HOME/.asdf/completions/asdf.bash"
fi

if [ -d "$HOME/.asdf/bin" ]; then
	export PATH="$PATH:$HOME/.asdf/bin"
fi
_EOF


~/.asdf/bin/asdf plugin list 2>/dev/null | grep nodejs -c &>/dev/null \
  || ~/.asdf/bin/asdf plugin add nodejs

~/.asdf/bin/asdf install nodejs 15.14.0


if [ ! -L /vagrant/node_modules ]; then
  cd /vagrant && ln -s ~/node_modules node_modules
fi
