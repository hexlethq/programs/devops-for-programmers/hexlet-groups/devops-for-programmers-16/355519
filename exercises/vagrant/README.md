# Vagrant

Создадим изолированное окружение для разработки с помощью Vagrant и запустим внутри него [Fastify](https://github.com/fastify/fastify). Fastify – микрофреймворк на JavaScript, который позволяет создать простой сайт используя буквально 20 строк кода (скопированных из документации).

## Ссылки

* [Что такое Vagrant](https://guides.hexlet.io/vagrant/)
* [Установка Node.js](https://github.com/nodesource/distributions#installation-instructions)
* [Vagrant: Проброс портов](https://www.vagrantup.com/docs/networking/forwarded_ports)

## Задачи

1. Установите [VirtualBox](https://www.virtualbox.org/wiki/Linux_Downloads)
2. Установите [Vagrant](https://www.vagrantup.com/docs/installation)
3. Инициализируйте Vagrant-проект

    ```sh
    vagrant init
    # image ubuntu/focal64.
    ```

4. Выставьте наружу 3000 порт. На нем запустится Fastify.
5. Установите Node.JS v15.x (JavaScript Runtime) с помощью [shell-скрипта](https://www.vagrantup.com/docs/provisioning/shell).
6. Инициализируйте Fastify-проект и убедитесь что он работает

    ```sh
    curl -fsSL https://deb.nodesource.com/setup_15.x | sudo -E bash -
		sudo apt-get install -y nodejs

    vagrant ssh
    cd /vagrant
    test -d ~/node_modules || mkdir  ~/node_modules
    ln -s ~/node_modules node_modules # Линкуем node_modules внутрь виртуальной машины.
    # Простой способ обойти ограничение на создание символических ссылок в общей директории.
    npm init -y fastify
    npm install
    FASTIFY_ADDRESS=0.0.0.0 npm run dev # проверяем что все работает
    # open localhost:3000
    ```

Результатом домашней работы будет установленный Vagrant, а также валидный Vagrantfile, в котором пробрасываются порты и происходит установка Node.JS.
# Подсказки

* Убедитесь, что установлена последняя версия npm (7+)
* В VirtualBox по соображениям безопасности отключена возможность создания символических ссылок в общедоступных каталогах, а каталог */vagrant* внутри виртуальной машины как раз таким и является. Можно просто залинковать *node_modules* внутрь виртуальной машины (как в примере выше), но правильней, чтобы корректно отработала команда `npm install` включить создание симлинков. В этом вам поможет статья — [VirtualBox 6: How to enable symlinks for shared folders](https://www.speich.net/articles/en/2018/12/24/virtualbox-6-how-to-enable-symlinks-in-a-linux-guest-os/)
* При выполнении *vagrant ssh* терминал подключается к виртуальной машине в домашнюю директорию пользователя, то есть */home/vagrant*, а код проекта появляется в каталоге */vagrant*
* Если мы используем Vagrant, то работа с проектом происходит внутри виртуальной машины, а не снаружи. Например, `npm install` выполняется после подключения к виртуальной машине по ssh и перехода в каталог с проектом.
