terraform {
  required_providers {
    digitalocean = {
      source  = "digitalocean/digitalocean"
      version = "~> 2.10.0"
    }
  }
}

provider "digitalocean" {
  token = var.do_token
}

variable "do_token" {
  type      = string
  sensitive = true
}

data "digitalocean_ssh_key" "default" {
  name = "mxpatlas vagrant"
}

resource "digitalocean_droplet" "web1" {
  name     = "web-terraform-homework-01"
  image    = "docker-20-04"
  region   = "ams3"
  size     = "s-1vcpu-1gb"
  ssh_keys = [data.digitalocean_ssh_key.default.id]
}

resource "digitalocean_droplet" "web2" {
  name     = "web-terraform-homework-02"
  image    = "docker-20-04"
  region   = "ams3"
  size     = "s-1vcpu-1gb"
  ssh_keys = [data.digitalocean_ssh_key.default.id]
}

resource "digitalocean_loadbalancer" "devops_app" {
  name   = "loadbalancer-1"
  region = "ams3"

  forwarding_rule {
    entry_port     = 80
    entry_protocol = "http"

    target_port     = 5000
    target_protocol = "http"
  }

  healthcheck {
    port                     = 5000
    protocol                 = "http"
    path                     = "/"
    response_timeout_seconds = 3
  }

  droplet_ids =[ digitalocean_droplet.web1.id, digitalocean_droplet.web2.id ]
}

#data "digitalocean_domain" "goresolv" {
#  name = "goresolv.art"
#}

resource "digitalocean_record" "devops_app_balancer" {
  domain = "goresolv.art"
  type   = "A"
  name   = "terra"
  value  = digitalocean_loadbalancer.devops_app.ip
	ttl = 300
}

output "droplet_ip_address" {
  value = [
    digitalocean_droplet.web1.ipv4_address,
    digitalocean_droplet.web2.ipv4_address,
  ]
}
